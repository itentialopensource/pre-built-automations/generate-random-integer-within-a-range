<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Generate Random Integer Within a Range

## Table of Contents

*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
* [Attributes](#attributes)
* [Additional Information](#additional-information)

## Overview

This JST allows IAP users to generate a random integer within a user provided range.

## Installation Prerequisites
Users must satisfy the following prerequisites:
* Itential Automation Platform : `^2022.1`

## How to Install

To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.
  

## How to Run

Use the following to run the pre-built:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the workflow where you would like to generate a random integer and add a `JSON Transformation` task.

2. Inside the `Transformation` task, search for and select `randomInteger` (the name of the internal JST).

3. The inputs to the JST would be the minimum and maximum numbers for the range (both inclusive) between which a random integer will be picked.

4. Save your input and the task is ready to run inside of IAP.

## Attributes
1. Input:
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>minimum</code></td>
<td>lower bound (inclusive) for the range between which a random integer will be generated</td>
<td><code>integer</td>
</tr>
<tr>
<td><code>maximum</code></td>
<td>upper bound (inclusive) for the range between which a random integer will be generated</td>
<td><code>integer</td>
</tr>
</tbody>
</table>


2. Output:
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>randomInteger</code></td>
<td>generated random integer</td>
<td><code>integer</td>
</tr>
</tbody>
</table>

## Additional Information
Please use your Itential Customer Succcess account if you need support when using this Pre-Built Transformation.
