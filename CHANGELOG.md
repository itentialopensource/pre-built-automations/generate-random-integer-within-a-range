
## 0.0.8 [01-30-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/generate-random-integer-within-a-range!9

---

## 0.0.7 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/generate-random-integer-within-a-range!8

---

## 0.0.6 [06-20-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/generate-random-integer-within-a-range!7

---

## 0.0.5 [11-29-2021]

* Update README.md for 2021.2

See merge request itentialopensource/pre-built-automations/generate-random-integer-within-a-range!6

---

## 0.0.4 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/generate-random-integer-within-a-range!4

---

## 0.0.3 [05-24-2021]

* 2021 certification

See merge request itentialopensource/pre-built-automations/generate-random-integer-within-a-range!3

---

## 0.0.2 [01-07-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/generate-random-integer-within-a-range!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
